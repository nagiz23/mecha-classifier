import os
# Function to rename multiple files
def main():
   i = 0
   path="TurboCharger"
   for filename in os.listdir(path):
      my_dest ="TurboCharger" + str(i) + "." + filename.rsplit('.', 1)[1]
      my_source =os.path.join(path, filename)
      my_dest =os.path.join(path , my_dest)
      # rename() function will
      # rename all the files
      os.rename(my_source, my_dest)
      i += 1
# Driver Code
if __name__ == '__main__':
   # Calling main() function
   main()
